import firebase from 'firebase';

// Ini inisialisai firebase kita
firebase.initializeApp( {
    apiKey: "AIzaSyBgFpuWMKer9p7F32rmUzw6hFNIwVrEtC8",
    authDomain: "my-doctor-01-195ac.firebaseapp.com",
    databaseURL: "https://my-doctor-01-195ac.firebaseio.com",
    projectId: "my-doctor-01-195ac",
    storageBucket: "my-doctor-01-195ac.appspot.com",
    messagingSenderId: "231900052533",
    appId: "1:231900052533:web:5561dce360450020db857c"
});

// Ini yang bisa digunakan di folder kita nanti
const Fire = firebase;

export default Fire;