const { useState } = require("react")

export const useForm = (initialvalue) => {
    // Jadi pada hook ini dia tidak hanya bisa mereturn component saja
    // Tetapi bisa juga return sebuah function
    const [values, setValues] = useState(initialvalue);
        return [
            values, 
            (formType,formValue) => {
                if(formType === 'reset'){
                    return setValues(initialvalue);
                }
                // Jadi kita ambil dulu value yang awal 
                // Kemudian kita ubah nilainya dengan params
                // Jangan lupa diberi function
                return setValues({...values , [formType] : formValue});
                // Yang dimana pada use from berisi formtype dan formvalue
            }, 
        ];
};