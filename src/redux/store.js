import {createStore} from 'redux';

// const [profile, setProfile] = useState();
//Konsep Dari store sama seperti usestate
//Bedanya store merupakan variable global

const initialState = {
    loading : false,
    name : 'Farabie',
    address : 'Pematang Gubernur',
}

const reducer = (state = initialState, action) => {
    if(action.type === 'SET_LOADING'){
        return {
            ...state,
            loading : action.value,
        };
    }
    if(action.type === 'SET_NAME'){
        return {
            ...state,
            name : 'Muhammad Farabie',
        }
    }
    return state;
};

const store = createStore(reducer);

export default store;