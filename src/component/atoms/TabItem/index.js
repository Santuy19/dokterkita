import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { 
    IconDoctor, 
    IconDoctorActive, 
    IconHospitals,
    IconHospitalsActive, 
    IconMessages, 
    IconMessagesActive, 
} from '../../../assets';
import { colors, fonts } from '../../../utils';



const Tabitem = ({title,active,onPress,onLongPress}) => {
    //Meletakan Icon membuat kondisi
    const Icon = () => {
        if ( title === 'Doctor' ){
            return active ? <IconDoctorActive /> : <IconDoctor />
        }
        if ( title === 'Messages' ){
            return active ? <IconMessagesActive /> : <IconMessages />
        }
        if ( title === 'Hospitals' ){
            return active ? <IconHospitalsActive /> : <IconHospitals />
        }
        return <IconDoctor />
    }
    return (
        <TouchableOpacity 
        style={styles.container} 
        onPress={onPress} 
        onLongPress={onLongPress}>
            <Icon />
            <Text style={styles.text(active)}>{title}</Text>
        </TouchableOpacity>
    )
}

export default Tabitem;
const styles = StyleSheet.create({
    container : {
        alignItems:'center',
    },
    text : (active) => (
        {
            fontSize : 10,
            fontFamily : fonts.primary[600],
            color: active ? colors.text.menuactive : colors.text.menuInactive,
            marginTop : 4,
        }
    ),
});
