import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import { colors, fonts } from '../../../utils'

const Input = ({label, value, onChangeText, secureTextEntry, disable}) => {
    // Membuat usestate agar border color ketika di klik bisa berubah
    const [border, setBorder] = useState (colors.border);
    const onFocusForm = () =>{
        setBorder(colors.tertiary);
    };
    const onBlurForm = () =>{
        // Memanggil lagi warna border awal
        setBorder(colors.border);
    }
    // Membuat Props on focus dan onchange agar terdapat props value dan onchange
    return (
        <View>
            <Text style={styles.label}>{label}</Text>
            {/* input akan menjadi sebuah function */}
            {/* Memanggil props on focus dan on blur form */}
            <TextInput 
            onFocus={onFocusForm} 
            onBlur={onBlurForm} 
            style={styles.input(border)}
            value={value}
            onChangeText={onChangeText}
            // Berfungsi untuk menyemunyikan password
            secureTextEntry={secureTextEntry}
            editable={!disable}
            selectTextOnFocus={!disable}
            />
        </View>
    )
}

export default Input;
const styles = StyleSheet.create({
    // Dijadikan sebuah function
    input : (border) => (
        {
            borderWidth: 1,
            borderColor : border,
            borderRadius : 10,
            padding : 12,
        }
    ), 
    label: {
        fontSize: 16,
        color: colors.text.secondary,
        marginBottom: 6,
        fontFamily: fonts.primary.normal,
    }
})
